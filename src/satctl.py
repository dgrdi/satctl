#!/usr/bin/env python3

import argparse
import logging
import asyncio
from cli.mcu import default_ctl

logger = logging.getLogger(__name__)

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    parser = argparse.ArgumentParser()
    parser.add_argument('--peak', help="Peak signal", action="store_true")
    parser.add_argument('--search', metavar="SATELLITE", help="Search satellite")
    parser.add_argument('--stow', help="Stow mount", action="store_true")
    parser.add_argument('--stop', help="Stop search", action="store_true")
    parser.add_argument('--signal', help="Show signal level", action="store_true")
    parser.add_argument('--teardown', help="Stow and uninstall modem", action="store_true")
    parser.add_argument('--setup', help="Setup modem and search satellite", action="store_true")
    parser.add_argument('--uninstall', help="Uninstall modem", action="store_true")
    parser.add_argument('--service', help="Move mount to service position", action="store_true")
    parser.add_argument('--gps', help="Override GPS coordinates")
    parser.add_argument('--angle', help="Step to use during peaking", type=float, default=0.4)
    parser.add_argument('--skew', help="Set HughesNet skew angle", action="store_true")
    parser.add_argument('--set-stow', action='store_true')
    parser.add_argument('--set-no-stow', action='store_true')

    args = parser.parse_args()

    ctl = default_ctl(args.gps)

    if args.stop:
        asyncio.run(ctl.stop())
    if args.stow:
        asyncio.run(ctl.stow())
    if args.search:
        asyncio.run(ctl.search(args.search, args.angle))
    if args.peak:
        asyncio.run(ctl.peak(args.angle))
    if args.uninstall:
        asyncio.run(ctl.uninstall())
    if args.teardown:
        asyncio.run(ctl.teardown())
    if args.setup:
        asyncio.run(ctl.setup(args.angle, args.skew))
    if args.skew and not args.setup:
        asyncio.run(ctl.set_skew())
    if args.signal:
        asyncio.run(ctl.log_signal())
    if args.service:
        asyncio.run(ctl.service())
    if args.set_stow:
        asyncio.run(ctl.set_stow(True))
    if args.set_no_stow:
        asyncio.run(ctl.set_stow(False))
