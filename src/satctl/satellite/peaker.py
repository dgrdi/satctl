from ..mount.position import Position


class DirectionPeaker:
    MIN_ANGLE = 0.1
    MAX_MOVES = 5

    def __init__(self, mount_position: Position, directions: (int, int), signal, sig_equals, sleep, logger):
        self.mount_position = mount_position
        self.signal = signal
        self.directions = directions
        self.sig_equals = sig_equals
        self.logger = logger
        self.sleep = sleep

    async def peak(self, angle):
        if angle < self.MIN_ANGLE:
            return
        max = prev = await self.signal()
        max_found = False
        dir = self.directions[0]
        moves = 0
        while True:
            await self.mount_position.move(dir, angle)
            await self.sleep(1)
            sig = await self.signal()
            if sig > max:
                max = sig
                if dir == self.directions[0]:
                    max_found = True
            self.logger.info(
                'Current signal: {:.2f}, previous: {:.2f}, max: {:.2f}, moved {} by {} degrees'.format(sig, prev, max,
                                                                                                       Position.name(
                                                                                                           dir), angle))
            if prev == sig:
                moves += 1
            else:
                moves = 0
            if dir == self.directions[1] and self.sig_equals(sig, max) and max_found:
                break
            if prev > sig or moves == self.MAX_MOVES:
                if dir == self.directions[0]:
                    dir = self.directions[1]
                    if moves == self.MAX_MOVES:
                        self.logger.info(
                            'No signal change, moving {} {} degrees'.format(Position.name(dir), angle * self.MAX_MOVES))
                        await self.mount_position.move(dir, angle * moves)
                        moves = 0
                else:
                    if prev > sig:
                        self.logger.info(
                            'Moving {} by {} degrees to correct last move'.format(Position.name(self.directions[0]),
                                                                                  angle))
                        await self.mount_position.move(self.directions[0], angle)
                    if moves == self.MAX_MOVES:
                        self.logger.info(
                            'No signal change, moving {} {} degrees'.format(Position.name(self.directions[0]),
                                                                            angle * self.MAX_MOVES))
                        await self.mount_position.move(self.directions[0], angle * moves)
                        return False
                    break
            prev = sig
        return True


class Peaker:
    def __init__(self, mount_position: Position, signal, sig_equals, sleep, getLogger):
        self.logger = getLogger(__name__)
        self.az_peaker = DirectionPeaker(mount_position, (mount_position.AZ_CW, mount_position.AZ_CCW), signal,
                                         sig_equals, sleep, self.logger)
        self.el_peaker = DirectionPeaker(mount_position, (mount_position.EL_DOWN, mount_position.EL_UP), signal,
                                         sig_equals, sleep, self.logger)

    async def peak(self, angle):
        self.logger.info("Peaking signal...")
        return await self._peak(angle, angle)

    async def _peak(self, el_angle, az_angle):
        if el_angle < DirectionPeaker.MIN_ANGLE and az_angle < DirectionPeaker.MIN_ANGLE:
            return True
        el, az = (True, True)
        new_el_angle, new_az_angle = el_angle, az_angle
        if el_angle >= DirectionPeaker.MIN_ANGLE:
            el = await self.el_peaker.peak(el_angle)
            if el:
                el_angle = el_angle / 2
        if az_angle >= DirectionPeaker.MIN_ANGLE:
            az = await self.az_peaker.peak(az_angle)
            if az:
                az_angle = az_angle / 2
        if not el and not az:
            return False
        if new_el_angle == el_angle and new_az_angle == az_angle:
            return False
        return await self._peak(el_angle, az_angle)
