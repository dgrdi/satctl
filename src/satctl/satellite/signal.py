from .client import ModemClient


class Signal:
    MIN_SIGNAL = 31
    SAMPLE_SIZE = 3
    INTERVAL = 0.1

    def __init__(self, sat: ModemClient, sleep):
        self.sat = sat
        self.sleep = sleep

    async def signal(self):
        tot = 0
        for i in range(self.SAMPLE_SIZE):
            tot += await self._get_signal(self.INTERVAL * i)
        return tot / self.SAMPLE_SIZE

    async def present(self):
        return await self._get_signal() >= self.MIN_SIGNAL

    async def decent(self):
        return await self._get_signal() > self.MIN_SIGNAL

    async def _get_signal(self, delay=0.0):
        if delay > 0:
            await self.sleep(delay)
        return (await self.sat.status_wan())['sat_rx_ss']
