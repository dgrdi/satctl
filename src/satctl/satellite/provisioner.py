from ..mount.gps import Gps
from ..satellite.client import ModemClient
from ..satellite.signal import Signal


class Provisioner:
    _interval = 1

    def __init__(self, gps: Gps, modem_client: ModemClient, signal: Signal, config, sleep, getLogger):
        self.modem_client = modem_client
        self.gps = gps
        self.signal = signal
        self._beams = None
        self.logger = getLogger(__name__)
        self.sleep = sleep
        self._satellites = {
            0: config['sat_0'],
            1: config['sat_1']
        }

    def satellite(self, sat_index):
        return self._satellites[sat_index]

    async def set_satellite(self, sat_index=0):
        await self.modem_client.reinstall()
        req = await self._gps_data()
        if sat_index > 0:
            req["input_sat_index"] = sat_index
        return await self.modem_client.input_data(req)

    async def get_beams(self):
        if self._beams is None:
            sat = 0
            ret = []
            while sat <= 1:
                beams = await self.set_satellite(sat)
                if 'beamSelectionErr' not in beams:
                    ret.append((sat, beams))
                sat += 1
            self._beams = ret
        return self._beams

    async def set_beam(self, satellite, beam):
        self.logger.info("Using satellite {}, beam {}, outroute {}".format(
            self.satellite(satellite),
            beam['beam'],
            beam['outroute']
        ))
        await self.set_satellite(satellite)
        await self.sleep(1)
        await self.modem_client.input_beam(beam)
        await self.sleep(1)
        return await self.modem_client.pointing_params()

    async def register(self, sat_id):
        beams = list(filter(lambda i: i[0] == sat_id, await self.get_beams()))[0][1]
        if len(beams) == 1:
            max_beam = beams[0]
        else:
            max_sig = None
            max_beam = None
            for beam in beams:
                await self.set_beam(sat_id, beam)
                await self.sleep(4)
                sig = (await self.signal.signal()) if (await self.signal.present()) else None
                self.logger.info("Signal is {}".format(sig))
                if sig is not None and (max_sig is None or sig > max_sig):
                    max_sig = sig
                    max_beam = beam
        if max_beam is not None:
            self.logger.info("Best signal using beam {}, outroute {}".format(max_beam['beam'], max_beam['outroute']))
            await self.set_beam(sat_id, max_beam)
            self.logger.info("Starting registration...")
            await self.modem_client.start_range()
        else:
            raise RuntimeError("Cannot find signal")

    async def _gps_data(self):
        lat, lon = await self.gps.coords()
        return {
            "input_lat_deg": lat,
            "input_lat_deg_deg": abs(int(lat)),
            "input_lat_min": (abs(lat) % 1) * 60,
            "input_lat_hem": 0 if lat >= 0 else 1,
            "input_lon_deg": lon,
            "input_lon_deg_deg": abs(int(lon)),
            "input_lon_min": (abs(lon) % 1) * 60,
            "input_lon_hem": 0 if lon >= 0 else 1,
        }
