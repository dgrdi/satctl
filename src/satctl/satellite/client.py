class ModemClient:
    def __init__(self, addr, transport):
        self.addr = addr
        self.transport = transport

    def path(self, path):
        return "{}/api/{}".format(self.addr, path)

    async def status_wan(self):
        return await self.transport.get(self.path('home/status/wan'))

    async def reinstall(self):
        return await self.transport.get(self.path('install/terminal_reinstall'))

    async def pointing_params(self):
        return await self.transport.get(self.path('install/pointing_parms'))

    async def input_data(self, data):
        defaults = {
            # "input_lat_deg": 39.334000016666664,
            # "input_lat_deg_deg": 39,
            # "input_lat_min": 20.040001,
            # "input_lat_hem": 0,
            # "input_lon_deg": -115.39060001666667,
            # "input_lon_deg_deg": 115,
            # "input_lon_min": 23.436001,
            # "input_lon_hem": 1,
            "skip_pointing": 0,
            "beam_overwrite": 1,
            "antennaSize": 74,
            "mobile_tail_num": "",
            "mobile_terminal": False,
            "mobile_chassis": "",
            "noModemBInstall": False,
            "use_ptp_mode": False,
            # "input_sat_index": 1
        }
        defaults.update(data)
        return await self.transport.post(self.path('install/input_data'), json=defaults)

    async def input_beam(self, beam):
        return await self.transport.post(self.path('install/beam_ort_choice'), json={
            "beam_choice": beam['beamChoice'],
            "outroute_choice": beam['outrouteIndex']
        })

    async def start_range(self):
        return await self.transport.get(self.path('install/start_range'))
