import re

from ..mount.client import MountClient


class SatelliteTable:
    sat_key = re.compile(r'Sat(\d+)')

    def __init__(self, mount_client: MountClient):
        self.mount_client = mount_client
        self._tbl = None

    async def load_table(self):
        if self._tbl is None:
            tbl = {}
            for key, value in (await self.mount_client.satellites()).items():
                m = self.sat_key.match(key)
                if m:
                    tbl[int(m.group(1))] = value
            self._tbl = tbl

    async def name(self, satellite_id):
        await self.load_table()
        return self._tbl[satellite_id]

    async def id(self, satellite_name):
        await self.load_table()
        found = []
        for key, value in self._tbl.items():
            if value.startswith(satellite_name):
                found.append(key)

        if len(found) == 0:
            raise KeyError("No satellite found with name '{}'".format(satellite_name))
        if len(found) > 1:
            raise KeyError("Ambiguous satellite name: '{}' matches {}".format(satellite_name,                                                                              ",".join(self.name(i) for i in found)))
        else:
            return found[0]
