from .client import MountClient


class Position:
    EL_UP, EL_DOWN, AZ_CW, AZ_CCW, SK_CCW, SK_CW = range(6)

    _play_correction = 0.2

    _directions = {
        EL_UP: 'ElUp',
        EL_DOWN: 'ElDn',
        AZ_CW: 'AzUp',
        AZ_CCW: 'AzDn',
        SK_CCW: 'SkDn',
        SK_CW: 'SkUp'
    }
    _signs = (
        (AZ_CW, AZ_CCW),
        (EL_UP, EL_DOWN),
        (SK_CW, SK_CCW)
    )

    _direction_names = {
        EL_UP: "elevation up",
        EL_DOWN: "elevation down",
        AZ_CW: "azimuth clockwise",
        AZ_CCW: "azimuth counterclockwise",
        SK_CCW: "skew counterclockwise",
        SK_CW: "skew clockwise"
    }

    @classmethod
    def name(cls, direction):
        return cls._direction_names[direction]

    def __init__(self, mount: MountClient):
        self.mount = mount
        self.last_az_move = None

    async def move(self, direction, degrees):
        # correct for play in azimuth movement
        if direction in (self.AZ_CW, self.AZ_CCW):
            if self.last_az_move is not None and direction != self.last_az_move:
                await self.raw_move(direction, self._play_correction)
                await self.raw_move(direction, self._play_correction)
            self.last_az_move = direction
        await self.raw_move(direction, degrees)

    async def stow(self):
        return await self._wait_until_put(await self.mount.stow())

    async def service(self):
        await self.stow()
        await self.raw_move(self.EL_UP, 90)
        await self.raw_move(self.AZ_CW, 10)

    async def raw_move(self, direction, degrees):
        await self._wait_until_put(await self.mount.move(self._directions[direction], degrees))


    async def position(self):
        """(Azimuth, Elevation, Skew)"""
        return tuple(map(float, (await self.mount.motor_status())['DishAngle'].split(',')))

    async def set_position(self, azimuth=None, elevation=None, skew=None):
        if elevation is not None and elevation < 80:
            raise ValueError("Elevation too low")
        prev_pos = await self.position()
        for p, prev_p, dirs in zip((azimuth, elevation, skew), prev_pos, self._signs):
            if p is None:
                continue
            diff = p - prev_p
            if diff == 0:
                continue
            dir = dirs[0] if diff > 0 else dirs[1]
            diff = abs(diff)
            await self.raw_move(dir, diff)



    async def _wait_until_put(self, motor_status):
        return await self.mount.wait_until_put(motor_status)
