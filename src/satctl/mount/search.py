import time

from ..mount.client import MountClient
from ..mount.sat_table import SatelliteTable


class Search:
    INTERVAL = 1
    TIMEOUT = 10 * 60 * 60

    def __init__(self, mount_client: MountClient, sat_table: SatelliteTable, signal_present, sleep, getLogger):
        self.mount_client = mount_client
        self.sat_table = sat_table
        self.signal_present = signal_present
        self.sleep = sleep
        self.logger = getLogger(__name__)

    async def rough_search(self, sat_name):
        start_time = time.time()
        self.logger.info("Starting search for {}".format(sat_name))
        await self.mount_client.start_search(await self.sat_table.id(sat_name))
        while time.time() - start_time < self.TIMEOUT:
            status = (await self.mount_client.status())['SysStatus']
            self.logger.info("Status: {}".format(status))
            status = status.lower()
            if status.startswith('optimizing'):
                self.logger.info("Satellite found, stopping search")
                await self.mount_client.wait_until_put(await self.mount_client.stop())
                return True
            if (await self.signal_present()) and status.startswith('peaking'):
                self.logger.info('Signal found, stopping search')
                await self.mount_client.wait_until_put(await self.mount_client.stop())
                return True
            if status.find('completed') >= 0:
                return True
            if status.find('failed') >= 0:
                return False
            await self.sleep(self.INTERVAL)
        self.logger.info("Cannot find satellite {}".format(sat_name))
        return False
