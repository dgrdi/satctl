class MountClient:
    _motor = 'motor.xml'
    _status = 'status.xml'
    _sat = 'usrsat.xml'

    def __init__(self, addr, transport):
        self.addr = addr
        self.transport = transport

    def path(self, path):
        return "{}/{}".format(self.addr, path)

    async def status(self):
        return await self.transport.get(self.path(self._status))

    async def motor_status(self):
        return await self.transport.get(self.path(self._motor))

    async def satellites(self):
        return await self.transport.get(self.path(self._sat))

    async def move(self, direction, degrees):
        return await self.transport.get(self.path(self._motor), params={
            'Action': direction,
            'Distance': degrees,
            'Units': 1
        })

    async def start_search(self, sat_id):
        return await self.transport.get(self.path(self._status), params={
            'Action': 'Search',
            'Satellite': sat_id
        })

    async def stop(self):
        return await self.transport.get(self.path(self._status), params={
            'Action': 'Stop'
        })

    async def stow(self):
        return await self.transport.get(self.path(self._status), params={
            'Action': 'Stow'
        })

    async def wait_until_put(self, status=None):
        while True:
            if status and status['State'] == '1':
                break
            status = await self.status()
