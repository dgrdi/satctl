import re


class Gps:

    def __init__(self, getLogger):
        self.logger = getLogger(__name__)

    async def raw_coords(self):
        raise NotImplementedError()

    def parse(self, raw_coords):
        mt = re.match('([0-9.]+)([NS]), ([0-9.+]+)([EW])', raw_coords)
        if mt is None:
            return None
        lat, lats, lon, lons = (mt.group(i) for i in range(1, 5))
        lat = float(lat)
        lon = float(lon)
        if lats == 'S':
            lat = -lat
        if lons == 'W':
            lon = -lon
        return lat, lon

    async def coords(self):
        coords = self.parse(await self.raw_coords())
        if coords is None:
            raise RuntimeError("Invalid GPS coordinates")
        return coords


class MountGps(Gps):
    _interval = 1

    def __init__(self, mount_client, sleep, getLogger):
        super().__init__(getLogger)
        self.mount_client = mount_client
        self._pos = None
        self.sleep = sleep

    async def raw_coords(self):
        while self._pos is None:
            pos = (await self.mount_client.status())['GPS']
            if not self.parse(pos):
                self.logger.info("Waiting for GPS...")
                await self.sleep(self._interval)
                continue
            self._pos = pos
        return self._pos


class ManualGps(Gps):
    def __init__(self, raw_coords, getLogger):
        super().__init__(getLogger)
        self.raw_coords = lambda: raw_coords
