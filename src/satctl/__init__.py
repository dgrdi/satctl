from satctl.mount.client import MountClient
from satctl.mount.gps import MountGps, ManualGps
from satctl.mount.position import Position
from satctl.mount.sat_table import SatelliteTable
from satctl.mount.search import Search
from satctl.satellite.client import ModemClient
from satctl.satellite.peaker import Peaker
from satctl.satellite.provisioner import Provisioner
from satctl.satellite.signal import Signal
import math

class SatCtl:
    def __init__(self, config, gps, polarization, json_transport, xml_transport, sleep, getLogger):
        self.logger = getLogger(__name__)
        self.config = config
        self.modem_client = ModemClient(self.config.get('modem_addr'), json_transport)
        self.mount_client = MountClient(self.config.get('mount_addr'), xml_transport)
        self.position = Position(self.mount_client)
        self.signal = Signal(self.modem_client, sleep)
        self.peaker = Peaker(self.position, self.signal.signal,
                             lambda a, b: abs(a - b) <= self.config.get('signal_tolerance', 0.5), sleep, getLogger)
        self.satellite_table = SatelliteTable(self.mount_client)
        self.searcher = Search(self.mount_client, self.satellite_table, self.signal.present, sleep, getLogger)
        self.gps = MountGps(self.mount_client, sleep, getLogger) if gps is None else ManualGps(gps, getLogger)
        self.provisioner = Provisioner(self.gps, self.modem_client, self.signal, self.config, sleep, getLogger)
        self.polarization = polarization(self.position, self.config)

    async def log_signal(self):
        self.logger.info("Signal is {:.2f}".format(await self.signal.signal()))

    async def uninstall(self):
        self.logger.info("Uninstalling modem...")
        await self.modem_client.reinstall()

    async def teardown(self):
        await self.stow()
        await self.uninstall()

    async def stop(self):
        await self.mount_client.stop()

    async def stow(self):
        self.logger.info("Stowing mount...")
        await self.mount_client.stow()

    async def peak(self, angle=0.4):
        ret = await self.peaker.peak(angle)
        await self.log_signal()
        return ret

    async def search(self, satellite, angle=0.4):
        if await self.searcher.rough_search(satellite):
            return await self.peak(angle)
        else:
            return False

    async def set_skew(self):
        skew = (await self.modem_client.pointing_params())['antenna_tilt']
        await self.position.set_position(skew=skew)


    async def setup(self, angle=0.4, set_skew=False):
        searched = None
        for satellite, beams in await self.provisioner.get_beams():
            for beam in beams:
                params = await self.provisioner.set_beam(satellite, beam)
                if await self.polarization.set_pol(params['uplink_pol']):
                    searched = None
                if searched != satellite:
                    sat_name = self.provisioner.satellite(satellite)
                    if not await self.searcher.rough_search(sat_name):
                        break
                    searched = satellite
                    if set_skew:
                        self.logger.info("Adjusting skew angle...")
                        await self.set_skew()
                if (await self.peak(angle)) and (await self.signal.decent()):
                    self.logger.info("Starting registration...")
                    await self.provisioner.register(satellite)
                    self.logger.info("Setup complete.")
                    return
        raise RuntimeError("Cannot find any satellite")

    async def service(self):
        self.logger.info("Moving mount to service position...")
        await self.position.service()
