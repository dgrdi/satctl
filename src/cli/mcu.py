from logging import getLogger

from .default import default_config

logger = getLogger(__name__)
import websockets
import json
import asyncio


def default_ctl(gps=None):
    if gps is not None:
        raise ValueError("GPS override not supported")
    return RemoteCtl(default_config()['mcu'])


class RemoteCtl:
    def __init__(self, addr):
        self.addr = addr

    async def log_signal(self):
        logger.info("Signal is {:.2f}".format(await self.send('signal')))

    async def uninstall(self):
        logger.info("Uninstalling modem...")
        await self.send('uninstall')

    async def teardown(self):
        logger.info("Tearing down...")
        await self.send('teardown')

    async def stop(self):
        await self.send('stop')

    async def stow(self):
        logger.info("Stowing mount...")
        await self.send('stow')

    async def peak(self, angle=0.4):
        await self.send('peak', angle=angle)
        await self.log_signal()

    async def search(self, satellite, angle=0.4):
        return await self.send('search', satellite=satellite, angle=angle)

    async def setup(self, angle=0.4, skew=False):
        await self.send('setup', angle=angle, skew=skew)

    async def service(self):
        logger.info("Moving mount to service position...")
        await self.send('service')

    async def set_skew(self):
        logger.info("Adjusting skew angle...")
        await self.send('set_skew')

    async def set_stow(self, stow):
        logger.info("Setting stow value...")
        await self.send('set_stow', stow=stow)

    async def send(self, cmd, **kwargs):
        try:
            async with websockets.connect(self.addr + '/satctl') as sock:
                req = {
                    'command': cmd
                }
                req.update(kwargs)
                await sock.send(json.dumps(req))
                resp = json.loads(await sock.recv())
                if resp['status'] == 'ERROR':
                    raise ValueError(resp['reason'])
                return resp.get('result', None)
        except (KeyboardInterrupt, asyncio.CancelledError):
            if cmd != 'stop':
                logger.info("Stopping operation")
                await self.stop()
            raise
