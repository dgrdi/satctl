import sys


class Polarization:
    _POL = 'polarization'

    def __init__(self, position, config):
        self.position = position
        self.config = config

    async def set_pol(self, pol):
        moved = False
        if self.config.get(self._POL) is None:
            resp = None
            while resp not in {'n', 'y'}:
                sys.stdout.write("Polarization needs to be {}. Is it (y/n)?\n".format(pol))
                sys.stdout.flush()
                resp = sys.stdin.readline().strip()
            if resp == 'y':
                self.config[self._POL] = pol

        if self.config.get(self._POL) != pol:
            moved = True
            await self.position.service()
            done = None
            while done != "done":
                sys.stdout.write("Enter \"done\" when polarization is set to {}\n".format(pol))
                sys.stdout.flush()
                done = sys.stdin.readline().strip()
        self.config[self._POL] = pol
        return moved
