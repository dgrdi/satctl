import logging
import re
import time

import requests as rq
from requests.adapters import HTTPAdapter

logger = logging.getLogger(__name__)

requests = rq.Session()

requests.mount('', HTTPAdapter(max_retries=5))



class Transport:
    def translate(self, response):
        raise NotImplementedError

    async def get(self, url, params=None):
        return self.retry(lambda: self.translate(requests.get(url, params=params)))

    async def post(self, url, json=None):
        return self.retry(lambda: self.translate(requests.post(url, json=json)))

    def retry(self, f):
        i = 0
        while True:
            try:
                return f()
            except:
                logger.info("Request failed, retrying...")
                i += 1
                if i == 3:
                    raise
                time.sleep(i)



class JsonTransport(Transport):

    def translate(self, response):
        try:
            response.raise_for_status()
            return response.json()
        except:
            logger.error("Expected JSON, instead got:")
            logger.error(response.content)
            logger.error("self.transport headers:")
            logger.error(response.self.transport.headers)
            logger.error("self.transport body:")
            logger.error(response.self.transport.body)
            raise


class XmlTransport(Transport):
    pat = re.compile('<([A-Za-z0-9]+)>(.*?)<.+?>')

    def parse_xml(self, xml):
        none, start, root, end_root, start_tag, tag, close_tag, val = 0, 1, 2, 3, 4, 5, 6, 7
        res = {}
        tbuf = bytearray()
        vbuf = bytearray()
        state = start
        for c in xml:
            if state == start:
                if c == ord('<'):
                    state = root
                continue
            if state == root:
                if c == ord('?'):
                    state = start
                if c == ord('>'):
                    state = none
                continue
            if state == none:
                if c == ord('<'):
                    state = start_tag
                continue
            if state == start_tag:
                if c == ord('/'):
                    state = none
                else:
                    tbuf.append(c)
                    state = tag
                continue
            if state == tag:
                if c == ord('>'):
                    state = val
                else:
                    tbuf.append(c)
                continue
            if state == val:
                if c == ord('<'):
                    state = close_tag
                    res[tbuf.decode('utf8')] = vbuf.decode('utf8')
                    tbuf.clear()
                    vbuf.clear()
                else:
                    vbuf.append(c)
                continue
            if state == close_tag:
                if c == ord('>'):
                    state = none
                continue
        return res

    def translate(self, response):
        try:
            response.raise_for_status()
            return self.parse_xml(response.content)
        except:
            logger.error("Expected XML, instead got:")
            logger.error(response.content)
            logger.error("Request headers:")
            logger.error(response.request.headers)
            logger.error("Request body:")
            logger.error(response.request.body)
            raise
