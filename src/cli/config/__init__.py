import json
import sys



class Config:
    _defaults = {
        'signal_tolerance': 0.5
    }

    def __init__(self, path):
        self.path = path
        self.load()

    def __getitem__(self, item):
        if item in self.config:
            return self.config[item]
        elif item in self._defaults:
            return self._defaults[item]
        else:
            raise KeyError("No such config key: \"{}\"".format(item))

    def __setitem__(self, key, value):
        self.config[key] = value
        self.save()

    def __delitem__(self, key):
        del self.config[key]
        self.save()

    def get(self, key, default=None):
        if key in self.config:
            return self.config.get(key, default)
        else:
            return self._defaults.get(key, default)

    def get_or_ask(self, key, description):
        value = self.get(key)
        if value is None:
            sys.stdout.write('Enter {}:\n'.format(description))
            value = sys.stdin.readline().strip()
            self[key] = value
        return value

    def load(self):
        try:
            with open(self.path, 'r') as cfile:
                self.config = json.load(cfile)
        except FileNotFoundError:
            self.config = {}

    def save(self):
        with open(self.path, 'w') as cfile:
            json.dump(self.config, cfile)
