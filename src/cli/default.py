import asyncio
import logging
import os

from cli.config import Config
from cli.polarization import Polarization
from cli.transport import JsonTransport, XmlTransport
from satctl import SatCtl

CONFIG = os.path.expanduser('~/.satctl_config.json')


def default_config():
    return Config(CONFIG)


def default_ctl(gps=None):
    return SatCtl(
        default_config(),
        gps,
        lambda *a: Polarization(*a),
        JsonTransport(),
        XmlTransport(),
        asyncio.sleep,
        logging.getLogger
    )
